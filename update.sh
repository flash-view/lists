#!/bin/bash

get_hochu_url () {
    url=`curl -s "http://cdntvnet.com/$1.php" -H 'Referer: http://hochu.tv/' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15' | grep "file:" | cut -d\" -f4`
}

channels="playboy:playboy
private-tv:private_hd
vivid-red:vivid_red
dorcel-tv:dorcel"
for channel in $channels
do
    name=`echo $channel | cut -d: -f1`
    path=`echo $channel | cut -d: -f2`

    get_hochu_url $name
    echo $url
    if [ ! -z "$url" ]; then
      sed -i -e "/$path/c\\$url" adult.m3u8
    fi
done

# dorcel_tv=`curl -s 'http://cdntvnet.com/dorcel-tv.php' -H 'Host: cdntvnet.com' -H 'Referer: http://hochu.tv/' | grep "file:" | cut -d\" -f4`
# echo $dorcel_tv
# sed -i "/dorcel/c\\$dorcel_tv" flash_adult.m3u8

get_only_tv_url () {
    url=`curl -s "http://cdnpotok.com/onlytv/$1.php" -H 'Referer: http://only-tv.org/' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15' | grep "file:" | cut -d\" -f4`
}

channels="viasat-history:vi_history:adult
viasat-explorer:vi_explorer:adult
viasat-nature:vi_nature:adult
travel-and-adventure:travel_adventure:adult
travel-channel:travel:adult
history:history:adult
national-geographic:nat_geogra:adult
nat-geo-wild:natgeowild:adult
okhota-i-rybalka:ohotaribalka:adult
zoo-tv:zootv:adult
morskoj:morskoi:adult
outdoor:outdoor:adult
plus-plus:plusplus:child
nick-jr:nickjr:child
piksel:piksel:child
jimjam:jimjam:child
tiji:tiji:child
disney:disney:child
nickelodeon:nickelodeon:child
tlum-hd:tlym:child
ani:ani:child"
for channel in $channels
do
    name=`echo $channel | cut -d: -f1`
    path=`echo $channel | cut -d: -f2`
    file=`echo $channel | cut -d: -f3`
    # echo $name $path $file

    get_only_tv_url $name
    echo $url
    if [ ! -z "$url" ]; then
      sed -i -e"/\/$path\//c\\$url" $file.m3u8
    fi
done

###
urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }
get_seetv_url () {
    urldecode `curl -s "http://seetv.tv/get/player/$1" -H "Referer: http://seetv.tv/vse-tv-online/$2" -H 'X-Requested-With: XMLHttpRequest' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15' | jq -r '.file'`
}
channels="10889:discovery-channeltv-online:57diskchy7
10905:discovery-science-tv:76dsincej8"
for channel in $channels
do
    id=`echo $channel | cut -d: -f1`
    ref=`echo $channel | cut -d: -f2`
    path=`echo $channel | cut -d: -f3`
    # echo $id $ref

    url=$( get_seetv_url $id $ref )
    echo $url
    if [ ! -z "$url" ]; then
      sed -i -e "/$path/c\\$url" adult.m3u8
    fi
done
